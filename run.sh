#!/bin/sh
set -eux
DIR=$(dirname "$0")
: "${WS:=$PWD}"

test -d "${WS}/.ssh" || mkdir -m 700 "${WS}/.ssh"
test -f "${WS}/.ssh/authorized_keys" || ssh-add -L >"${WS}/.ssh/authorized_keys"
test -f "${WS}/.pam_environment" || printf 'XDG_RUNTIME_DIR=/run/user/0\n' >"${WS}/.pam_environment"
test -f "${WS}/.profile" || printf 'PATH="${PATH}:/usr/lib/llvm-15/bin"\n' >"${WS}/.profile"

IMAGE_ID=$(cd "$DIR" && podman image build -f - . <./Dockerfile | tee /dev/stderr | tail -n 1)

# https://github.com/moby/moby/issues/19464#issuecomment-173269279=
#	--mount="type=bind,src=${SSH_AUTH_SOCK},dst=/run/user/0/openssh_agent" \
exec podman container run --init --rm --shm-size=64MiB \
  --cap-add=IPC_LOCK,NET_RAW,SYS_PTRACE --security-opt=seccomp=unconfined \
  --mount='type=tmpfs,dst=/tmp,rw' \
  --mount='type=bind,src=/dev,dst=/host_dev,rw' \
  --mount="type=bind,src=${DBUS_SESSION_BUS_ADDRESS#*=},dst=/run/user/0/bus" \
  --mount="type=bind,src=$(gpgconf --list-dirs socketdir),dst=/run/user/0/gnupg" \
  --mount="type=bind,src=${WS},dst=/root,rw" \
  -p 2222:2222/tcp \
  -p 3000:3000/tcp \
  -p 5000:5000/tcp \
  --name=devenv "$IMAGE_ID"
