# v0.1
FROM ubuntu:22.04 AS devenv

ENV LC_CTYPE=C.UTF-8 POSIXLY_CORRECT=y
ARG TZ=UTC
ARG DEBIAN_FRONTEND=noninteractive

COPY ./trust/. /tmp

RUN printf 'LC_CTYPE=C.UTF-8\nPOSIXLY_CORRECT=y\n' >>/etc/environment
RUN apt-get -q update && apt-get -qy --no-install-recommends install apt-transport-https ca-certificates

RUN . /etc/os-release && \
  base64 -d </tmp/ppa_fish.txt >/usr/share/keyrings/ppa_fish-archive-keyring.gpg && \
  printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    ppa_fish 'http://ppa.launchpadcontent.net/fish-shell/release-3/ubuntu' "$UBUNTU_CODENAME" \
    >'/etc/apt/sources.list.d/ppa_fish.list'

RUN . /etc/os-release && \
  base64 -d </tmp/ppa_git.txt >/usr/share/keyrings/ppa_git-archive-keyring.gpg && \
  printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    ppa_git 'http://ppa.launchpadcontent.net/git-core/ppa/ubuntu' "$UBUNTU_CODENAME" \
    >'/etc/apt/sources.list.d/ppa_git.list'

RUN . /etc/os-release && \
  base64 -d </tmp/ppa_openjdk.txt >/usr/share/keyrings/ppa_openjdk-archive-keyring.gpg && \
  printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    ppa_openjdk 'http://ppa.launchpadcontent.net/openjdk-r/ppa/ubuntu' "$UBUNTU_CODENAME" \
    >'/etc/apt/sources.list.d/ppa_openjdk.list'

RUN . /etc/os-release && \
 base64 -d </tmp/ppa_deadsnakes.txt >/usr/share/keyrings/ppa_deadsnakes-archive-keyring.gpg && \
 printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    ppa_deadsnakes 'http://ppa.launchpadcontent.net/deadsnakes/ppa/ubuntu' "$UBUNTU_CODENAME" \
    >'/etc/apt/sources.list.d/ppa_python.list'

RUN . /etc/os-release && \
  base64 -d </tmp/kitware.txt >/usr/share/keyrings/kitware-archive-keyring.gpg && \
  printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    kitware 'https://apt.kitware.com/ubuntu' "$UBUNTU_CODENAME" \
    >'/etc/apt/sources.list.d/kitware.list'

RUN . /etc/os-release && \
  base64 -d </tmp/llvm.txt >/usr/share/keyrings/llvm-archive-keyring.gpg && \
  printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    llvm "https://apt.llvm.org/${UBUNTU_CODENAME}" "llvm-toolchain-${UBUNTU_CODENAME}-15" \
    >'/etc/apt/sources.list.d/llvm.list'

RUN . /etc/os-release && \
  base64 -d </tmp/node.txt >/usr/share/keyrings/node-archive-keyring.gpg && \
  printf 'deb [signed-by=/usr/share/keyrings/%s-archive-keyring.gpg] %s %s main\n' \
    node 'http://deb.nodesource.com/node_16.x' "$UBUNTU_CODENAME" \
    >'/etc/apt/sources.list.d/node.list'

RUN apt-get -q update && apt-get -qy --no-install-recommends install \
  diffutils file findutils gzip jq less nano patch pax stow tree unzip zstd \
  bind9-dnsutils inotify-tools iproute2 iputils-ping lsof netcat procps socat strace \
  curl dbus fish gdb git git-lfs git-review gpg ssh telnet \
  autoconf automake cmake libtool make ninja-build pkg-config \
  nodejs openjdk-17-jre-headless python3.11-minimal libpython3.11-stdlib \
  clang-tools-15 clang-format-15 clangd-15 clang-tidy-15 lldb-15 \
  clang-15 lld-15 llvm-15 libc++-15-dev libc++abi-15-dev libunwind-15-dev

###

VOLUME /root

RUN mkdir -p -m 700 /run/user/0 && touch /run/user/0/flatpak-info
RUN mkdir /run/sshd && echo 'PasswordAuthentication no' >>/etc/ssh/sshd_config
RUN sed -i 's:/usr/bin/bash:/bin/sh:g;s:/bin/bash:/bin/sh:g' /etc/passwd

EXPOSE 2222/tcp
CMD ["/usr/sbin/sshd", "-De", "-p", "2222"]

###

FROM devenv
RUN apt-get -qy --no-install-recommends install \
  liblldb-15-dev yacc \
  libexpat1-dev libdbus-1-dev \
  libgmp-dev \
  libopus-dev gnutls-dev uuid-dev \
  libargon2-dev libasio-dev libfmt-dev libhttp-parser-dev libmsgpack-dev librestinio-dev libssl-dev gnutls-dev nettle-dev \
  libopus-dev libx264-dev libva-dev libffmpeg-nvenc-dev zlib1g-dev nasm \
  libarchive-dev libfmt-dev libgit2-dev libpulse-dev libssl-dev libudev-dev libwebrtc-audio-processing-dev gnutls-dev nettle-dev zlib1g-dev
ENV PATH="${PATH}:/usr/lib/llvm-15/bin"
RUN env CXX=clang++ npm -q install -g llnode node-gyp
RUN (curl -s 'https://codeload.github.com/swig/swig/tar.gz/ad1688055dea4f07cd4023d5e0301d97f7759151' | gzip -d | pax -r) && \
  (cd ./swig-* && ./autogen.sh && ./configure --without-pcre && make -j "$(nproc)" && make install)
