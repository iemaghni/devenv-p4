Install VSCode, then:
# apt install git openssh-client podman
$ ssh-keygen
$ mkdir ~/ws
$ cd ~/ws
$ git clone https://gitab.com/iemaghni/devenv-p4.git
$ ./devenv-p4/run.sh

In VSCode, connect to remote SSH "ssh://root@localhost:2222":
$ git clone https://review.jami.net/jami-daemon
$ git clone https://review.jami.net/jami-web

$ cd ./jami-daemon
$ patch -Np1 <../devenv-p4/t.patch
$ ../devenv-p4/build.sh

$ cd ../jami-web
$ env CXX=clang++ npm install
$ npm run build
$ ln -s ../jami-daemon/bin/nodejs/build/Release/jamid.node .
$ export LD_LIBRARY_PATH=../jami-daemon/src/.libs
$ export SECRET_KEY_BASE=test123
$ npm run start

Open http://localhost:3000 in your browser.
