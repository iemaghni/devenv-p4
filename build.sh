#!/bin/sh
set -eux

N=$(nproc)
N=$((N-1))
# N=1

build_contrib() {
  env CC=clang ../bootstrap

  make -j "$N" CC=clang CXX=clang++ .jsoncpp .yaml-cpp .dbus-cpp .natpmp .upnp .speex .speexdsp

  make -j "$N" CC=clang DEPS_secp256k1= .secp256k1

  # FUCK YOU HALF-ASSED AUTOTOOLS: OPUS support disabled AND YET YOU FFS IMPORT THE HEADER
  # FUCK YOU HALF-ASSED AUTOTOOLS: SETTING CC AND CXX BRAKE THE BUILD
  ln -s "$(command -v clang)" /usr/local/bin/gcc || :
  ln -s "$(command -v clang++)" /usr/local/bin/g++ || :
  make -j "$N" DEPS_pjproject= .pjproject
  rm /usr/local/bin/gcc /usr/local/bin/g++

  make -j "$N" CC=clang CXX='clang++ -std=c++17' DEPS_opendht= .opendht

  make -j "$N" CC=clang DEPS_ffmpeg= .ffmpeg
}

build_jamid() {
  autoreconf -i .
  ./configure --disable-static --with-pic --without-alsa --with-nodejs CC=clang CXX='clang++ -std=c++17' LIBS='-lgnutls -luuid -largon2' "CPATH=${PWD}/contrib/x86_64-pc-linux-gnu/include" "LIBRARY_PATH=${PWD}/contrib/x86_64-pc-linux-gnu/lib"
  # Automatic detection? Don't know. Hired.
  make -j "$N" CXX='clang++ -std=c++17'
}

rmmkcd() { rm -Rf "$1" && mkdir "$1" && cd "$1"; }

(rmmkcd './contrib/native' && build_contrib)

build_jamid

printf '\n*** done ***\n'
